from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from app.posts.api.views import PostViewSet
from app.posts.api.views import UserViewSet

router = DefaultRouter() if settings.DEBUG else SimpleRouter()
router.register("post", PostViewSet)
router.register("users", UserViewSet, basename="users")


app_name = "api"
urlpatterns = router.urls
