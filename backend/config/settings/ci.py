
import os
from .base import *  # noqa: F401, F403
SECRET_KEY = os.getenv('SECRET_KEY', default="Cu96ky2qXk9yxxd4CrDOlQpFrCTJNl31sxtxBhEfpVBDxe6SGOlkUUuFT7nqk654")
ALLOWED_HOSTS = ["*"]
DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': os.getenv('POSTGRES_ENGINE'),
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT', '5432'),
    }
}

