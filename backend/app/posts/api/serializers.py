from django.contrib.auth import get_user_model
from rest_framework import serializers
from app.posts import models


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            "id",
            "body",
            "created_at",
            "updated_at",
            "author",
            "socialmedia",
            "is_queue",
            "is_send",
            "video",
            "image",
        )
        model = models.Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            "id",
            "username",
        )
