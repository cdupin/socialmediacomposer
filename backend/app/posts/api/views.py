from rest_framework.permissions import AllowAny
from django.contrib.auth import get_user_model
from rest_framework.viewsets import ModelViewSet
from app.posts.models import Post
from app.posts.api.serializers import PostSerializer
from app.posts.api.serializers import UserSerializer
from app.posts.permissions import IsAuthorOrReadOnly


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthorOrReadOnly,)


class UserViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
