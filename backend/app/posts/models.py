from django.db import models
from django.contrib.auth.models import User


class SocialMedia(models.Model):
    socialmedia = models.CharField(max_length=255)

    def __str__(self):
        return self.socialmedia


class Video(models.Model):
    file = models.FileField(blank=False, null=False)
    description = models.CharField(max_length=255)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file


class Image(models.Model):
    file = models.FileField(blank=False, null=False)
    description = models.CharField(max_length=255)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file


class Post(models.Model):

    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    socialmedia = models.ForeignKey(SocialMedia, on_delete=models.CASCADE)
    is_queue = models.BooleanField(default=True)
    is_send = models.BooleanField(default=False)
    video = models.ManyToManyField(
        Video, related_name="video", blank=True, null=True
    )
    image = models.ManyToManyField(
        Image, related_name="image", blank=True, null=True
    )

    def __str__(self):
        return self.author.username
