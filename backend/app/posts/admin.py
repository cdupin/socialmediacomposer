from django.contrib import admin
from .models import Post
from .models import SocialMedia
from .models import Video
from .models import Image

admin.site.register(Post)
admin.site.register(SocialMedia)
admin.site.register(Video)
admin.site.register(Image)
