#!/bin/sh
#test
ssh -o StrictHostKeyChecking=no cdupin@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
  cd /home/cdupin/socialmediacomposer
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:django
  docker pull $IMAGE:nginx
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH
